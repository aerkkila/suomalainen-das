Tällä asennetaan toisinto suomalaisesta das-näppäimistöasettelusta ja tehdään siitä oletusasettelu.

Varoitus: Tämä muuttaa näppäimistön oletusasettelua.

XKB:ta käyttävässä ympäristössä (Xorg, Wayland) ajetaan xkb.sh

Linux-konsolilla ajetaan konsoli.sh. Arch-linuxilla johdannaisineen toimii suoraan, mutta Ubuntulla pitää ensin asentaa paketti console-data.

Kyseiset komentosarjat xkb.sh ja konsoli.sh lataavat tarvittaessa muut tiedostot tästä hakemistosta.
